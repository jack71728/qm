#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <assert.h>
#include <stdbool.h>
#include <signal.h>
#include <inttypes.h>
#include <float.h>
#include <ctype.h>
#include <time.h> 
/* Global declarations */
#define NMAX 12 /* max number of variables */
#define NTERMS 4096 /* 2 to the NMAX */
#define QTERMS 256 /* NTERMS divided by WORD */
#define MTERMS 4096 /* maximum numbers of minterms */
/* allowed */
#define MIMPS 48 /* largest allowable number of */
/* prime implicants */
#define LIN 1200 /* longest input line size */
#define BIG 32000 /* short integer infinity */
#define WORD 16 /* short integer size */
#define gbit(a,b) (((a)>>(b))&1)

void QMmain(char *data, int var_num, int *minterms, int *dontcares);

void getterms(int var_num, char *minterms_str, char *dontcares_str);
void ttable(void);
void ntrmv(int i,char *entry,int trm);
void kmap(int var_num, char *minterms_str, char *dontcares_str);
void expres(void);
void quine(void);//
void pairup(int first,int last);//
int bitcount(int len,int term);//
void primes(int first,int last);//
void primepath(int j,int imp);//
int pchart(void);
char *reduction(int uncov);

struct list {
	int term;
	int mom; /* first ancestor */
	int dad; /* second ancestor */
	int nocare;
	int match;
} plist[MTERMS*2];
