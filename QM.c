#include "QM.h"

//======================================header

int nvars; /* number of variables in case */
int nterms = 1; /* numbers of terms in case */
int nwords; /* nterms/WORD + 1 */
int minterm[QTERMS]; /* minterm array (Karnaugh map) */
int noterm[QTERMS]; /* no cares in Karnaugh map */
int impchart[QTERMS][MIMPS]; /* prime implicant chart */
int impcnt[QTERMS]; /* indicates coverage of a minterm */
int impext[QTERMS]; /* indicates multiple coverage of term */
int essprm[MIMPS]; /* marker for essential prime implicants*/
int imps = 0; /* number of prime implicants */
int priterm[MIMPS]; /* term values for prime implicants */
int pricare[MIMPS]; /* nocare values for prime implicants */
int pptr; /* number of actual nterms */
char func = 'f'; /* function name */
char vname[NMAX]; /* variable name list */
//char *fgets();
FILE *fp;
char bla[40000];

void QMmain(char *data, int var_num, int *minterms, int *dontcares)
{
	int i, converted_num;
	converted_num = 1 << var_num;
	fp = fopen("log", "w+");
	char minterms_str[20000] = "\0", dontcares_str[20000] = "\0";
	char buffer[100], *answer;
	
	nvars = 0; /* number of variables in case */
	nterms = 1; /* numbers of terms in case */
	nwords = 0; /* nterms/WORD + 1 */
	memset(minterm, 0, sizeof(int) * QTERMS); /* minterm array (Karnaugh map) */
	memset(noterm, 0, sizeof(int) * QTERMS); /* no cares in Karnaugh map */
	memset(impchart, 0, sizeof(int) * QTERMS *MIMPS); /* prime implicant chart */
	memset(impcnt, 0, sizeof(int) * QTERMS);/* indicates coverage of a minterm */
	memset(impext, 0, sizeof(int) * QTERMS); /* indicates multiple coverage of term */
	memset(essprm, 0, sizeof(int) * MIMPS); /* marker for essential prime implicants*/
	imps = 0; /* number of prime implicants */
	memset(priterm, 0, sizeof(int) * MIMPS); /* term values for prime implicants */
	memset(pricare, 0, sizeof(int) * MIMPS); /* nocare values for prime implicants */
	pptr = 0; /* number of actual nterms */
	func = 'f'; /* function name */
	memset(vname, 0, sizeof(char) * NMAX); /* variable name list */
	memset(bla, 0, sizeof(char) * 40000);
	memset(data, 0, sizeof(char) * 40000);
	
	for(i = 0; i < converted_num; ++i)
	{
		if(*(minterms+i) == 1)
		{
			//printf("minterms_str got %d\n", i);
			sprintf(buffer, "%d ", i);
			strcat(minterms_str, buffer);
		}
	}
	sprintf(buffer, "\n");
	strcat(minterms_str, buffer);
	//puts("minterms_str");
	//puts(minterms_str);
	for(i = 0; i < converted_num; ++i)
	{
		if(*(dontcares+i) == 1)
		{
			sprintf(buffer, "%d ", i);
			strcat(dontcares_str, buffer);
		}
	}
	sprintf(buffer, "\n");
	strcat(dontcares_str, buffer);
	//puts("dontcares_str");
	//puts(dontcares_str);
	
	//printf("minterms_str = %s qqqq dontcares_str = %s\n", minterms_str, dontcares_str);

	
	/* input the functions to be reduced */
	getterms(var_num, minterms_str, dontcares_str);
	if ( (nvars > 0 ) && (nvars <= NMAX) ) {
		/* generate Quine-McCluskey reduction */
		quine();
		/* set up prime implicant chart and determine essential primes */
		i = pchart(); 
		/* determine minimum function */
		reduction(i);
	}
	//getchar();
	strcpy(data, bla);
	//printf("inQM, data = %s, bla = %s\n", data, bla);
	return ;
}
void getterms(int var_num, char *minterms_str, char *dontcares_str)
{
	char iinline[LIN];
	register int i=0, j, temp;
	int format;
	int all = 1, none = 0; /* degenerate function checks */
	while ( i < QTERMS ) minterm[i++] = 0;
	/* prompt for format of input */
	/*fprintf(fp,"Welcome to the Dallen-Rogers switching function\n");
	fprintf(fp,"minimization program, Version 1.0, Dec 1, 1981.\n");
	fprintf(fp,"Enter the number for your preferred input type.\n");
	fprintf(fp," 1 - truth table\n");
	fprintf(fp," 2 - decimal codes for Karnaugh mapping\n");
	fprintf(fp," 3 - logical expression\n");
	fprintf(fp,"Format number? ");
	fgets(iinline,LIN,stdin);
	sscanf(iinline,"%d",&format);
	if ( (format < 1) || (format > 3) ) {
		fprintf(fp,"I don't understand your selection. Please try again.\n");
		fprintf(fp,"format number (1 to 3) ? ");
		fgets(iinline,LIN,stdin);
		sscanf(iinline,"%d",&format);
		if ( (format < 1) || (format > 3) ) {
			fprintf(stderr,"invalid input\n");
			nvars = 0;
			return;
		}
	}*/
	format = 2;
	//puts("minterms~");
	switch (format) {
		case 1:
			ttable();
			break;
		case 2:
			kmap(var_num, minterms_str, dontcares_str);
			break;
		case 3:
			expres();
			break;
	}
	/* check for degenerate cases */
	temp = WORD;
	if ( nterms < WORD ) temp = nterms;
	for ( i=0; i<nwords; i++) {
		none |= minterm[i];
		for ( j=0; j<temp; j++ ) {
			all &= gbit(minterm[i]|noterm[i],j);
		}
	}
	if ( all != 0 ) {
		fprintf(fp,"\nfunction = 1\n");
		nvars = 0;
	} else if ( none == 0 ) {
		fprintf(fp,"\nfunction = 0\n");
		nvars = 0;
	}
	return;
}
void ttable()
{
	char iinline[LIN];
	char entry[WORD];
	register int i=0, j, k;
	char *gptr;
	/* instructions for inputting the truth table */
	fprintf(fp,"Enter each row of your truth table, with input\n");
	fprintf(fp,"values as 0, 1 or X (not-cares) plus an output\n");
	fprintf(fp,"value of 0 or 1. Enter an extra RETURN after the\n");
	fprintf(fp,"last line of the truth table. Delimiters between\n");
	fprintf(fp,"column entries are not required\n");
	fprintf(fp,"Row 1: ");
	gptr = fgets(iinline,LIN,stdin);
	/* Read first line of input to determine the number */
	/* of variables involved */
	nvars = 0;
	i = 0;
	while ( iinline[i] != '\n') {
		if ( (iinline[i]=='1')||(iinline[i]=='0')||(iinline[i]=='x')||
				(iinline[i]=='X') ) {
			entry[nvars++] = iinline[i];
		}
		i++;
	}
	nvars--;
	/* Determine the maximum numbers of minterms possible */
	/* and the amount of storage required */
	for ( i=0; i<nvars; i++ ) {
		nterms *= 2; 
	}
	nwords = ((nterms+(WORD-1))/WORD);
	/* set up default variable names */
	for ( i=nvars-1; i>=0; i-- ) {
#ifdef JMH
		vname[i] = (char)('A' + nvars - i - 1);
#else
		vname[i] = (char)('z' - nvars + i + 1);
#endif
	}
	/* set up defaults as not-cares */
	i = 0;
	while ( i < nwords ) {
		for ( j=0; j<MIMPS; j++ ) impchart[i][j] = 0;
		noterm[i] = ~0;
		minterm[i++] = 0;
	}
	/* zero out unused bits */
	if (nterms < WORD ) {
		for ( j=nterms; j<WORD; j++ ) {
			noterm[0] &= ~(1<<j);
		}
	}
	/* Process each line of truth table and input the next line */
	k = 2;
	while ( (gptr != NULL) && (iinline[0] != '\n') ) {
		i = 0;
		j = 0;
		while ( iinline[i] != '\n') {
			if ( (iinline[i]=='1')||(iinline[i]=='0')||(iinline[i]=='x')||
					(iinline[i]=='X') ) {
				entry[j++] = iinline[i];
			}
			i++;
		}
		ntrmv(0,entry,0);
		fprintf(fp,"Row %d: ",k++);
		gptr = fgets(iinline,LIN,stdin);
	}
	return;
}
void ntrmv(int i,char *entry,int trm)
//	register int i; /* column number from input table */
//	char *entry; /* pointer to next input term in input string */
//	register int trm; /* current minterm configuration */
{ 
	int num;
	char iterm;
	/* have all the input terms been processed ? */
	if ( i >= nvars ) {
		sscanf(entry,"%d",&num);
		minterm[trm/WORD] |= num<<(trm%WORD);
		noterm[trm/WORD] &= ~(1<<(trm%WORD));
	} else {
		sscanf(entry,"%1s",&iterm);
		if ( (iterm == '1') || (iterm == '0') ) {
			trm = (trm<<1)|(iterm-'0');
			ntrmv(i+1,entry+1,trm);
		} else {
			/* any printed symbols other than 0 or 1 are treated */
			/* as not-care terms. */
			trm = trm<<1;
			ntrmv(i+1,entry+1,trm);
			trm |= 1;
			ntrmv(i+1,entry+1,trm);
		}
	}
	return;
}
void kmap(int var_num, char *minterms_str, char *dontcares_str)
{
	char *iinline;
	char entry[WORD];
	register int i=0, j;
	int num;
	char *gptr;
	fprintf(fp,"Input the number of variables in your function: ");
	//fgets(iinline,LIN,stdin);
	//sscanf(iinline,"%d",&nvars);
	nvars = var_num;
	/* Check for valid number of variables */
	if ( nvars <= 0 ) {
		nvars = 0;
		return;
	}
	if ( nvars > NMAX ) {
		fprintf(fp,"Too many variables, %d max\n",NMAX);
		nvars = 0;
		return;
	}
	/* Determine maximum number of minterms and required storage */
	for ( i=0; i<nvars; i++ ) { 
		nterms *= 2;
	}
	nwords = ((nterms+(WORD-1))/WORD);
	for ( i=0; i<nwords; i++ ) {
		for ( j=0; j<MIMPS; j++ ) impchart[i][j] = 0;
	}
	/* set up default variable names */
	for ( i=nvars-1; i>=0; i-- ) {
#ifdef JMH
		vname[i] = (char)('A' + nvars - i - 1);
#else
		vname[i] = (char)('z' - nvars + i + 1);
#endif
	}
	fprintf(fp,"On one line, input each minterm number ");
	fprintf(fp,"(0 to %d) separated by spaces or tabs:\n",nterms-1);
	//gptr = fgets(iinline,LIN,stdin);
	gptr = minterms_str;
	iinline = minterms_str;
	if ( (gptr != NULL) && (iinline[0] == '\n') ) {
		nvars = 0;
		return;
	}
	/* Process minterms */
	i = 0;
	while (sscanf(&iinline[i],"%s",entry) != EOF ) {
		j = 0;
		while ( entry[j++] != 0 );
		i += j;
		sscanf(entry,"%d", &num );
		minterm[num/WORD] |= (1<<(num%WORD));
	}
	/* Process not care terms */
	fprintf(fp,"Input each not-care number (0 to %d), if any:\n",nterms-1);
	//gptr = fgets(iinline,LIN,stdin);
	gptr = dontcares_str;
	iinline = dontcares_str;
	//printf("dontcares_str = %s\n", dontcares_str);
	//puts("kkkkkkkkkkmap");
	if ( (gptr != NULL) && (iinline[0] != '\n') ) {
		i = 0;
		while (sscanf(&iinline[i],"%s",entry) != EOF ) {
			j = 0;
			while ( entry[j++] != 0 );
			i += j;
			sscanf(entry,"%d", &num );
			noterm[num/WORD] |= (1<<(num%WORD));
		}
		//printf("naf = %d\n", noterm[0]);
	}
	return;
}
void expres() 
{
	char iinline[LIN];
	char outline[LIN];
	char tterm[WORD];
	char vdig;
	register int i=0, j=0, k=0;
	int flag = 0;
	char *gptr;
	fprintf(fp,"Input a logical expression, in sum of products form,\n");
	fprintf(fp,"using single upper or lower case letters as\n");
	fprintf(fp,"variable names (no subscripts) and a ' following the\n");
	fprintf(fp,"variable name to indicate a complement. Use a + between\n");
	fprintf(fp,"terms. Spaces, extraneous symbols, and anything\n");
	fprintf(fp,"in front of an optional equal sign are ignored.\n? ");
	gptr = fgets(iinline,LIN,stdin);
	/* check for bad input */
	nvars = 0;
	if ((gptr==NULL) || (iinline[0]=='\n')) {
		nvars = 0;
		fprintf(stderr,"no input expression, program terminated\n");
		return;
	}
	/* process expression: eliminate white space, count variables, */
	/* and reverse position of complement sign. */
	while (iinline[i] != '\n') {
		if ( iinline[i++] == '=' ) flag = i;
	}
	i = flag;
	while ( iinline[i] != '\n' ) {
		if ( iinline[i] == '+' ) {
			outline[j++] = iinline[i];
		} else if ( iinline[i] == '\'' ) {
			/* reverse complement sign position */
			outline[j] = outline[j-1];
			outline[j-1] = iinline[i];
			j++;
		} else if (((iinline[i]>='a')&&(iinline[i]<='z')) ||
				((iinline[i]>='A')&&(iinline[i]<='Z'))) {
			/* check to see if a new variable */
			outline[j++] = iinline[i];
			flag = 0;
			for ( k=0; k<nvars; k++ ) {
				if ( vname[k] == iinline[i] ) flag = 1;
			}
			if ( flag == 0 ) {
				vname[nvars++] = iinline[i];

			}
		}
		i++;
	}
	/* j is length of outline */
	outline[j++] = '+';
	/* Determine maximum number of minterms and required storage */
	for ( i=0; i<nvars; i++ ) {
		nterms *= 2;
	}
	nwords = ((nterms+(WORD-1))/WORD);
	/* set up defaults as zero terms */
	i = 0;
	while ( i < nwords ) {
		for ( k=0; k<MIMPS; k++ ) impchart[i][k] = 0;
		noterm[i] = 0;
		minterm[i++] = 0;
	}
	/* check for valid expressions */
	if ( nvars == 0 ) {
		fprintf(stderr,"Expression not in readable form\n");
		return;
	}
	/* Convert variables to minterms */
	k = 0;
	while ( k < j ) {
		tterm[nvars] = '1'; /* Sum of products term */
		for ( i=0; i<nvars; i++ ) tterm[i] = 'X';
		while ( outline[k] != '+' ) {
			/* build truth table term */
			if ( outline[k] == '\'' ) {
				vdig = '0';
				k++;
			} else vdig = '1';
			for ( i=0; i<nvars; i++ ) {
				if ( outline[k] == vname[i] ) {
					tterm[i] = vdig;
				}
			}
			k++;
		}
		/* set up minterms */
		ntrmv(0,tterm,0);
		k++;
	} 
	return;
}
void quine()
{
	register int i, j;
	/* initialize prime implicant count */
	for ( j=0; j<nwords; j++ ) {
		impcnt[j] = 0;
		impext[j] = 0;
	}
	for ( j=0; j<MIMPS; j++ ) {
		essprm[j] = 0;
	}
	/* set up pairings list */
	for ( i=0; i<nterms; i++ ) {
		if (gbit((minterm[i/WORD]|noterm[i/WORD]),i%WORD) == 1 ) {
			plist[pptr].nocare = 0;
			plist[pptr].match = 0;
			plist[pptr].mom = 1;
			plist[pptr].dad = 0;
			plist[pptr++].term = i;
			if ( pptr >= MTERMS ) {
				fprintf(stderr,"Too many minterms ( > %d )\n",MTERMS);
				fprintf(stderr,"Process aborted\n");
				i = nterms;
				pptr = 0; /* nullify process */
			}
		}
	}
	/* process pairings */
	pairup(0,pptr);
	return;
}
void pairup(int first,int last)
//	int first, last; /* pointers to first term and last term ( + 1 ) */
	/* of candidate terms at one level of Q-M reduc-*/
	/* tion. */
{
	int match = 0; /* indicates a pairing was found */
	int submatch = 0; /* pairing found on one pass */
	int diff, diffx = 0; /* nocare term variables */
	int fterm, dterm; /* first term in loop parameters */
	register int next; /* pointer to next available plist location */
	int jstart, second; /* pointers within the level */
	int j2;
	register int j, k; 
	jstart = first;
	second = first;
	next = last; /* initialize loop controls */
	j = jstart;
	j2 = jstart;
	while ( jstart< last-1 ) {
		while ( j2 < (last-1) ) {
			for ( k=second+1; k<last; k++ ) {
				/* At this point, the full series of Quine-McCluskey 'tests' */
				/* are made to see if a pairing can be made. */
				if ((plist[j].nocare == plist[k].nocare ) &&
						(bitcount(nvars,plist[j].term)
						 == (bitcount(nvars,plist[k].term)-1)) &&
						(bitcount(nvars,diff=(plist[k].term^plist[j].term))==1) ) {
					if ((diffx==0)||((((plist[j].term-fterm)%dterm) == 0) &&
								(diff==diffx))){
						/* A pairing has been made. Record the pair at the */
						/* next level. */
						match = 1;
						submatch = 1;
						if ( diffx == 0 ) {
							dterm = plist[k].term-plist[j].term;
							fterm = plist[j].term;
						}
						plist[j].match = 1;
						plist[k].match = 1;
						plist[next].match = 0;
						plist[next].nocare = plist[j].nocare|diff;
						plist[next].term = plist[j].term;
						plist[next].mom = j;
						plist[next++].dad = k;
						second = k;
						diffx = diff;
						j = ++k;
					}
				}
			}
			/* A series of tests is made to limit the number of */
			/* possible pairings (without forgetting any), in */
			/* order to accomplish the tabulation recursively. */
			if ( submatch == 1 ) {
				second += 2;
				j = second;
				submatch = 0; 
			} else {
				j = j+1;
				j2 = j;
				second = j;
			}
		}
		if ( match == 1 ) {
			/* go to the next level of tabulation */
			pairup(last,next);
			j2 = plist[last].mom;
			j = j2;
			second = plist[last].dad;
			next = last;
			match = 0;
			diffx = 0;
		} else {
			jstart++;
			second = jstart;
			j = jstart;
		}
	}
	/* process the candidate prime implicant */
	primes(first,last);
	return;
}
int bitcount(int len,int term)
//	int len; /* length of string to be counted */
//	register int term; /* string to be counted */
{
	register int i;
	register int count = 0;
	for ( i=0; i< len; i++ ) count+=(term>>i)&1;
	return count;
}
void primes(int first,int last)
//	int first, last;
{
	register int i, j;
	int flag;
	int rep;
	/* output prime implicants */
	for ( j=first; j<last; j++ ) {
		if ( plist[j].match == 0 ) {
			flag = 0;
			for ( i=0; i<imps; i++ ) {
				/* test to see if candidate is a subset of a larger prime imp */ 

				if (bitcount(nvars,plist[j].nocare) <= bitcount(nvars,pricare[i]) ){
					if (((plist[j].nocare|pricare[i]) == (pricare[i])) &&
							(((~pricare[i])&priterm[i])==((~pricare[i])&plist[j].term))&&
							((plist[j].term|priterm[i]|pricare[i]) ==
							 (priterm[i]|pricare[i]))) {
						flag = 1;
					}
					/* test to see if candidate will replace a smaller subset */
				} else if
					(bitcount(nvars,plist[j].nocare)>bitcount(nvars,pricare[i])) {
						if (((pricare[i]|plist[j].nocare)==(plist[j].nocare)) &&
								(((~plist[j].nocare)&plist[j].term) ==
								 ((~plist[j].nocare)&priterm[i])) &&
								((priterm[i]|plist[j].term|plist[j].nocare) ==
								 (plist[j].term|plist[j].nocare))) {
							flag = 2;
							rep = i;
						}
					}
			}
			/* Add a prime implicant to list--no complications */
			if (flag == 0) {
				primepath(j,imps);
				priterm[imps] = plist[j].term;
				pricare[imps] = plist[j].nocare;
				imps++;
				if ( imps >= MIMPS ) {
					fprintf(stderr,"Warning, overflow of prime implicant chart\n");
					imps--; /* for protection */
				}
				/* Preform the replacement of a prime implicat with a larger one */
			} else if (flag == 2) {
				primepath(j,rep);
				priterm[rep] = plist[j].term;
				pricare[rep] = plist[j].nocare;
			}
		}
	}
	return;
}
void primepath(int j,int imp)
//	register int j; /* start node in Quine-McCluskey */
//	register int imp; /* entry in implicant table */
{
	if ( j < pptr ) {
		/* arrival back at original terms */
		impchart[plist[j].term/WORD][imp] |= (1<<(plist[j].term%WORD));
	} else {
		primepath(plist[j].mom,imp);
		primepath(plist[j].dad,imp);
	}
	return;
}
int pchart()
{
	register int i, j, k;
	int uncov;
	int temp;
	char echar;
	/* determine coverage of minterms */
	for ( i=0; i<nwords; i++ ) {
		for ( j=0; j<imps; j++ ) {
			impcnt[i] |= impchart[i][j];
		}
	}
	/* determine multiple coverage of minterms */
	for ( i=0; i<nterms; i++ ) {
		temp = 0;
		for ( j=0; j<imps; j++ ) {
			temp += (gbit(impchart[i/WORD][j],i%WORD));
		}
		if ( temp >= 2 ) impext[i/WORD] |= (1<<(i%WORD));
	}
	/* exclude not-care terms from consideration */
	for ( i=0; i<nwords; i++ ) {
		/* eliminate not-care cases */
		impcnt[i] &= ~noterm[i];
		impext[i] &= ~noterm[i];
		/* check for prime implicants */
		for ( j=0; j<WORD; j++ ) {
			if ( (gbit(impcnt[i],j) == 1) && (gbit(impext[i],j)==0) ) {
				k = 0;
				while ( gbit(impchart[i][k],j) == 0 ) k++;
				essprm[k] = 1;
			}
		}
	}
	/* Determine coverage of essential prime implicants and */
	/* print out prime implicants */
	fprintf(fp,"\nPrime implicants ( * indicates essential prime implicant )");
	for ( i=0; i<imps; i++ ) { 
		if ( essprm[i] == 1 ) {
			echar = '*';
			for ( j=0; j<nwords; j++ ) {
				impcnt[j] &= ~impchart[j][i];
			}
		} else echar = ' ';
		fprintf(fp,"\n%c ",echar);
		for ( j=0; j<nvars; j++ ) {
			if (((pricare[i]>>(nvars-1-j))&1) == 0) {
				fprintf(fp,"%c",vname[j]);
				if (((priterm[i]>>(nvars-1-j))&1) == 0) {
					fprintf(fp,"'");
				}
			}
		}
		fprintf(fp,"\t: ");
		for ( j=0; j<nterms; j++ ) {
			if ( gbit(impchart[j/WORD][i],j%WORD) == 1 )
				fprintf(fp,"%d,",j);
		}
	}
	uncov = 0;
	for ( i=0; i<nwords; i++ ) {
		uncov += bitcount(WORD,impcnt[i]);
	}
	return(uncov);
}
char *reduction(int uncov)
//	int uncov;
{
	register int i,j;
	char buffer[100];
	bla[0] = 0;
	int nonemps; /* number of non-essential terms */
	int terms, lits; /* minimization factors */
	int nons[MIMPS]; /* index into impchart of non-essential impl.*/
	int termlist[QTERMS]; /* temporary location of covered term count */
	int fail; /* new candidate flag */
	long limit, li; /* power set bits */
	char oper; /* sum of products separator */
	/* current best coverage candidate */
	struct current {
		int terms;
		int lits;
		int list[MIMPS];
	} curbest;
	if ( uncov == 0 ) {
		fprintf(fp,"\n\nminimal expression is unique\n"); 
	} else {
		fprintf(fp,"\n\nno unique minimal expression\n");
		/* set up non-essential implicant list */
		j = 0;
		for ( i=0; i<imps; i++ )
			if (essprm[i] == 0) nons[j++] = i;
		nonemps = j;
		/* insure no overflow of cyclical prime implicant array */
		if ( nonemps > 2*WORD ) {
			fprintf(stderr,"Warning! Only %d prime implicants can be",2*WORD);
			fprintf(stderr,"considered for coverage\n of all terms (in addition");
			fprintf(stderr,"to essential primes). %d implicants not checked\n",
					nonemps-(2*WORD));
			nonemps = 2*WORD;
		}
		if ( nonemps > WORD ) {
			fprintf(fp,"Warning! Large number of cyclical prime implicants\n");
			fprintf(fp,"Computation will take awhile\n");
		}
		/* candidate coverage is determined by generation of the power set */
		/* calculate power set */
		limit = 1;
		for ( i=0; i<nonemps; i++ ) limit *= 2;
		/* set up current best expression list */
		curbest.terms = BIG;
		curbest.lits = BIG;
		curbest.list[0] = -1;
		/* try each case */
		for ( li=1L; li<limit; li++ ) {
			terms = bitcount(2*WORD,li);
			if ( terms <= curbest.terms ) {
				/* reset count */
				lits = 0;
				/* reset uncovered term list */
				for ( i=0; i<nwords; i++ )
					termlist[i] = impcnt[i];
				for ( i=0; i<nonemps; i++ ) {
					if (((li>>i)&1L) == 1L) {
						for ( j=0; j<nterms; j++ ) {
							if (gbit(impchart[j/WORD][nons[i]],j%WORD)==1) {
								termlist[j/WORD] &= ~(1<<(j%WORD));
							}
						}
					}
					lits += (nvars - bitcount(nvars,pricare[nons[i]]));
				} 
				fail = 0;
				for ( i=0; i<nwords; i++ ) {
					if ( termlist[i]!=0 ) fail = 1;
				}
				if ((fail==0) && ((terms<curbest.terms) || (lits<curbest.lits))) {
					/* we have a new candidate */
					curbest.terms = terms;
					curbest.lits = lits;
					j = 0;
					for ( i=0; i<nonemps; i++ ) {
						if (((li>>i)&1L)==1L) {
							curbest.list[j++] = nons[i];
						}
					}
					curbest.list[j] = -1;
				}
			}
		}
		j = 0;
		while ( curbest.list[j] >= 0 ) {
			essprm[curbest.list[j]] = 1;
			j++;
		}
	}
	/* print out minimal expression */
	fprintf(fp,"\nminimal expression:\n\n %c(",func);
	for ( i=0; i<nvars; i++ ) {
		fprintf(fp,"%c",vname[i]);
		if ( i < nvars - 1 ) fprintf(fp,",");
	}
	fprintf(fp,")");
	oper = '=';
	for ( i=0; i<imps; i++ ) {
		if ( essprm[i] == 1 ) {
			fprintf(fp," %c ",oper);
			for ( j=0; j<nvars; j++ ) {
				if (((pricare[i]>>(nvars-1-j))&1) == 0) {
					fprintf(fp,"%c",vname[j]);
					if (((priterm[i]>>(nvars-1-j))&1) == 0) {
						fprintf(fp,"'");
					}
				}
			}
			oper = '+';
		} 
	}
	fprintf(fp,"\n ... minimal expression:\n\n %c(",func);
	for ( i=1; i<=nvars; i++ ) {
		if(i <= nvars-1)
			fprintf(fp,"Y%d",i);
		else
			fprintf(fp,"X");
		if ( i < nvars ) fprintf(fp,",");
	}
	fprintf(fp,")");
	oper = '=';
	for ( i=0; i<imps; i++ ) {
		if ( essprm[i] == 1 ) {
			fprintf(fp," %c ",oper);
			if(oper != '=')
			{
				sprintf(buffer," %c ",oper);
				strcat(bla, buffer);
			}
			for ( j=0; j<nvars; j++ ) {
				if (((pricare[i]>>(nvars-1-j))&1) == 0) {
					if(j < nvars-1)
					{
						fprintf(fp,"Y%d",j+1);
						sprintf(buffer,"Y%d",j+1);
						strcat(bla, buffer);
					}
					else
					{
						fprintf(fp,"X");
						sprintf(buffer, "X");
						strcat(bla, buffer);
					}
					if (((priterm[i]>>(nvars-1-j))&1) == 0) {
						fprintf(fp,"'");
						sprintf(buffer, "'");
						strcat(bla, buffer);
					}
				}
			}
			oper = '+';
		} 
	}
	
	fprintf(fp,"\n\n");
	//sprintf(buffer, "");
	//strcat(bla, buffer);
	return bla;
} 
