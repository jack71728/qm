#include<stdio.h>
#include<string.h>
#include<math.h>
#include "QM.h"

int main(){
	int i,l;
	int v_num;//number of variables
	int y_num;//number of y
	int y_d[6][32] = {{0}};
	int y0[6][32] = {{0}};//input = 0
	int y1[6][32] = {{0}};//input = 1
	int *y_0;
	int *y_1;
	//flip-flop table
	int s[4] = {0,1,0,2};
	int r[4] = {2,0,1,0};
	int j[4] = {0,1,2,2};
	int k[4] = {2,2,1,0};
	int t[4] = {0,1,1,0};
	int d[4] = {0,1,0,1};
	
	int temp1[32] = {0};
	int temp2[32] = {0};
	int temp_m[64] = {0};
	int temp_d[64] = {0}; 
	int z1[32] = {0};
	int z2[32] = {0};
	char type[10];
	char answer[40000];
	int c = 0;
	
	scanf("%d",&v_num);
	scanf("%d",&y_num);
	scanf("%s",type);
	
	for(i = 0;i < v_num;i++){
		for(l = y_num;l > 0;l--)
			scanf("%d",&(y0[l][i]));
		scanf("%d",&(z1[i]));
		for(l = y_num;l > 0;l--)
			scanf("%d",&(y1[l][i]));
		scanf("%d",&(z2[i]));
			
		}
	/*for(i = 0;i < 5;i++)
		printf("%c\n",type[i]);*/
	
	for(i = 1;i < 32;i++){
		 y_d[1][i] = y_d[1][i-1];
		 y_d[2][i] = y_d[2][i-1];
		 y_d[3][i] = y_d[3][i-1];
		 y_d[4][i] = y_d[4][i-1];
		 y_d[5][i] = y_d[5][i-1];
		 
		y_d[1][i]++;
		if(y_d[1][i] == 2){
			y_d[1][i] = 0;
			y_d[2][i]++;
			if(y_d[2][i] == 2){
				y_d[2][i] = 0;
				y_d[3][i]++;
				if(y_d[3][i] == 2){
					y_d[3][i] = 0;
					y_d[4][i]++;
					if(y_d[4][i] == 2){
						y_d[4][i] = 0;
						y_d[5][i]++;
						}
					}
				}
			}
		}
	for(i = 1;i <= y_num;i++){
		
		for(l = 0;l < 32;l++){
			temp1[l] = 2;
			temp2[l] = 2;
			}
		for(l = 0;l < 64;l++){
			temp_m[l] = 0;
			temp_d[l] = 0;
			}
		//SR flip-flop
		if((type[i-1] == 's')||(type[i-1] == 'S')){
			for(l = 0;l < v_num;l++){
				temp1[l] = s[(2*y_d[i][l])+y0[i][l]];
				temp2[l] = s[(2*y_d[i][l])+y1[i][l]];
				}
			for(l = 0;l < 32;l++){
				if(temp1[l] == 1)
					temp_m[2*l] = 1;
				else if(temp1[l] == 2)
					temp_d[2*l] = 1;
				if(temp2[l] == 1)
					temp_m[2*l+1] = 1;
				else if(temp2[l] == 2)
					temp_d[2*l+1] = 1;
				
				}
			/*for(l = 0; l < 64; ++l)
				printf("temp_m[%d] = %d\n", l, temp_m[l]);*/
			/*for(l = 0;l < 64;l++)
				if(temp_m[l] == 1)
					c = 1;
			if(c = 0)
				printf("GGGGGGGGGGGGG\n");*/
				
			QMmain(answer, y_num+1, temp_m, temp_d);
			if(strlen(answer) == 0)
				printf("S%d = 0\n",y_num-i+1);
			else
				printf("S%d = %s\n",y_num-i+1,answer);
			for(l = 0;l < 32;l++){
				temp1[l] = 2;
				temp2[l] = 2;
				}
			for(l = 0;l < 64;l++){
				temp_m[l] = 0;
				temp_d[l] = 0;
				}
			for(l = 0;l < v_num;l++){
				temp1[l] = r[(2*y_d[i][l])+y0[i][l]];
				temp2[l] = r[(2*y_d[i][l])+y1[i][l]];
				}
			for(l = 0;l < 32;l++){
				if(temp1[l] == 1)
					temp_m[2*l] = 1;
				else if(temp1[l] == 2)
					temp_d[2*l] = 1;
				if(temp2[l] == 1)
					temp_m[2*l+1] = 1;
				else if(temp2[l] == 2)
					temp_d[2*l+1] = 1;
				
				}
			QMmain(answer, y_num+1, temp_m, temp_d);
			if(strlen(answer) == 0)
				printf("R%d = 0\n\n",y_num-i+1);
			else
				printf("R%d = %s\n\n",y_num-i+1,answer);
			}
		//JK flip-flop
		if((type[i-1] == 'j')||(type[i-1] == 'J')){
			for(l = 0;l < v_num;l++){
				temp1[l] = j[(2*y_d[i][l])+y0[i][l]];
				temp2[l] = j[(2*y_d[i][l])+y1[i][l]];
				}
			for(l = 0;l < 32;l++){
				if(temp1[l] == 1)
					temp_m[2*l] = 1;
				else if(temp1[l] == 2)
					temp_d[2*l] = 1;
				if(temp2[l] == 1)
					temp_m[2*l+1] = 1;
				else if(temp2[l] == 2)
					temp_d[2*l+1] = 1;
				
				}
			QMmain(answer, y_num+1, temp_m, temp_d);
			if(strlen(answer) == 0)
				printf("J%d = 0\n",y_num-i+1);
			else
				printf("J%d = %s\n",y_num-i+1,answer);
			for(l = 0;l < 32;l++){
				temp1[l] = 2;
				temp2[l] = 2;
				}
			for(l = 0;l < 64;l++){
				temp_m[l] = 0;
				temp_d[l] = 0;
				}
			for(l = 0;l < v_num;l++){
				temp1[l] = k[(2*y_d[i][l])+y0[i][l]];
				temp2[l] = k[(2*y_d[i][l])+y1[i][l]];
				}
			for(l = 0;l < 32;l++){
				if(temp1[l] == 1)
					temp_m[2*l] = 1;
				else if(temp1[l] == 2)
					temp_d[2*l] = 1;
				if(temp2[l] == 1)
					temp_m[2*l+1] = 1;
				else if(temp2[l] == 2)
					temp_d[2*l+1] = 1;
				
				}
			QMmain(answer, y_num+1, temp_m, temp_d);
			if(strlen(answer) == 0)
				printf("K%d = 0\n\n",y_num-i+1);
			else
				printf("K%d = %s\n\n",y_num-i+1,answer);
			}
		//D flip-flop
		if((type[i-1] == 'd')||(type[i-1] == 'D')){
			for(l = 0;l < v_num;l++){
				temp1[l] = d[(2*y_d[i][l])+y0[i][l]];
				temp2[l] = d[(2*y_d[i][l])+y1[i][l]];
				}
			for(l = 0;l < 32;l++){
				if(temp1[l] == 1)
					temp_m[2*l] = 1;
				else if(temp1[l] == 2)
					temp_d[2*l] = 1;
				if(temp2[l] == 1)
					temp_m[2*l+1] = 1;
				else if(temp2[l] == 2)
					temp_d[2*l+1] = 1;
				
				}
			QMmain(answer, y_num+1, temp_m, temp_d);
			if(strlen(answer) == 0)
				printf("D%d = 0\n\n",y_num-i+1);
			else
				printf("D%d = %s\n\n",y_num-i+1,answer);
			}
		//T flip-flop
		if((type[i-1] == 't')||(type[i-1] == 'T')){
			for(l = 0;l < v_num;l++){
				temp1[l] = t[(2*y_d[i][l])+y0[i][l]];
				temp2[l] = t[(2*y_d[i][l])+y1[i][l]];
				}
			for(l = 0;l < 32;l++){
				if(temp1[l] == 1)
					temp_m[2*l] = 1;
				else if(temp1[l] == 2)
					temp_d[2*l] = 1;
				if(temp2[l] == 1)
					temp_m[2*l+1] = 1;
				else if(temp2[l] == 2)
					temp_d[2*l+1] = 1;
				
				}
			QMmain(answer, y_num+1, temp_m, temp_d);
				if(strlen(answer) == 0)
					printf("T%d = 0\n\n", y_num-i+1);
				else
					printf("T%d = %s\n\n",y_num-i+1,answer);
			}
		
		}
	for(l = 0;l < 64;l++){
		temp_m[l] = 0;
		temp_d[l] = 0;
		}
	for(l = 0;l < 32;l++){
		if(z1[l] == 1)
			temp_m[2*l] = 1;
		if(z2[l] == 1)
			temp_m[2*l+1] = 1;
		}
	//temp_m[0] = 1;
	QMmain(answer, y_num+1, temp_m, temp_d);
	if(strlen(answer) == 0)
		puts("Z = 0");
	else
		printf("Z = %s\n",answer);
	/*for(i = 0;i < 4;i++)
		printf("%d\n",s[i]);
	for(i = 0;i < 32;i++)
		printf("%d%d%d%d%d\n",y_d[5][i],y_d[4][i],y_d[3][i],y_d[2][i],y_d[1][i]);*/
	return 0;
}